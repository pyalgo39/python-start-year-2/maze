# создай игру "Лабиринт"!
""" Необходимые модули """
from pygame import *


""" Классы для спрайтов"""


# основной класс для всех спрайтов
class GameSprite(sprite.Sprite):
    """
    image_file - имя файла с картинкой для спрайта
    x - координата x спрайта
    y - координата y спрайта
    speed - скорость спрайта
    """

    # конструктор
    def __init__(self, image_file, x, y, speed):
        super().__init__()  # конструктор суперкласса
        self.image = transform.scale(
            image.load(image_file), (65, 65)
        )  # создание внешнего вида спрайта - картинки
        # все спрайты размера 65*65
        self.speed = speed  # скорость
        self.rect = (
            self.image.get_rect()
        )  # прозрачная подложка спрайта - физическая модель
        self.rect.x = x
        self.rect.y = y

    # метод для отрисовки спрайта
    def reset(self):
        # отображает картинку в координатах физической модели
        window.blit(self.image, (self.rect.x, self.rect.y))


# класс для игрока (наследник от основного класса)
class Player(GameSprite):
    # метод для управления спрайтом
    def update(self):
        keys = key.get_pressed()  # набор всех нажатых клавиш

        # если нажата W и физическая модель не ушла за ВЕРХНЮЮ границу
        if keys[K_w] and self.rect.y > 5:
            # двигаем его вверх
            self.rect.y -= self.speed
        # если нажата S и физическая модель не ушла за НИЖНЮЮ границу
        if keys[K_s] and self.rect.y < height - 70:
            # двигаем его вниз
            self.rect.y += self.speed

        # если нажата A и физическая модель не ушла за ЛЕВУЮ границу
        if keys[K_a] and self.rect.x > 5:
            # двигаем его влево
            self.rect.x -= self.speed
        # если нажата D и физическая модель не ушла за ПРАВУЮ границу
        if keys[K_d] and self.rect.x < width - 70:
            #  двигаем его вправо
            self.rect.x += self.speed


# класс для врага (наследник от основного класса)
class Enemy(GameSprite):
    direction = "left"  # начальное направление

    # метод для автоматического передвижения
    def update(self):
        # если достиг левой точки
        if self.rect.x <= 470:
            # меняем направление движения на вправо
            self.direction = "right"
        # если достиг правой точки
        if self.rect.x >= width - 70:
            # меняем направление движения на влево
            self.direction = "left"

        # если направление движения влево
        if self.direction == "left":
            # двигаем влево
            self.rect.x -= self.speed
        else:
            # иначе двигаем вправо
            self.rect.x += self.speed


# класс для спрайтов-стен
class Wall(sprite.Sprite):
    def __init__(self, r, g, b, x, y, width, height):
        super().__init__()
        # параметры цвета в R G B
        self.r = r
        self.g = g
        self.b = b
        # размеры стены
        self.width = width
        self.height = height
        # картинка стены - прямоугольник нужных размеров и цвета
        self.image = Surface((self.width, self.height))
        self.image.fill((r, g, b))  # заливаем цветом
        # получаем "физическую" модель стены
        self.rect = self.image.get_rect()
        # размещаем её в нужные координаты
        self.rect.x = x
        self.rect.y = y

    # метод для отрисовки стены
    def draw_wall(self):
        # отображает картинку в координатах физической модели
        window.blit(self.image, (self.rect.x, self.rect.y))


# размеры окна игры
width = 700
height = 500

# создание окна с названием
window = display.set_mode((width, height))
display.set_caption("Лабиринт")

# создание фона сцены
background = transform.scale(image.load("background.png"), (width, height))

# переменная окончания игры
finish = False  # когда True, то спрайты перестают работать
# переменная завершения программы
game = True  # завершается при нажатии кнопки закрыть окно

# внутриигровые часы и ФПС
clock = time.Clock()
FPS = 60

# # музыка и звуки
mixer.init()
# фоновая музка
mixer.music.load("jungles.ogg")
mixer.music.play()

# звуки
kick = mixer.Sound("kick.ogg")  # звук удара
money = mixer.Sound("money.ogg")  # звук монет

# создание надписей
font.init()
my_font = font.Font(None, 70)
win = my_font.render("YOU WIN!", True, (255, 215, 0))
lose = my_font.render("YOU LOSE!", True, (180, 0, 0))

# создание трех игровых объектов
player = Player("hero.png", 5, height - 80, 4)  # игрок
monster = Enemy("cyborg.png", width - 80, 280, 2)  # противник
final = GameSprite("treasure.png", width - 120, height - 80, 0)  # сокровище

# создание стенок
w1 = Wall(121, 207, 217, 100, 20, 450, 10)
w2 = Wall(121, 207, 217, 100, 480, 350, 10)
w3 = Wall(121, 207, 217, 100, 20, 10, 380)
w4 = Wall(121, 207, 217, 200, 130, 10, 350)
w5 = Wall(121, 207, 217, 450, 130, 10, 360)
w6 = Wall(121, 207, 217, 300, 20, 10, 350)
w7 = Wall(121, 207, 217, 390, 120, 130, 10)

# пока игровой цикл (как программы) работает
while game:
    # обработка события «клик по кнопке "Закрыть окно"»
    for e in event.get():
        if e.type == QUIT:
            game = False  # завершение игрового цикла (как программы)

    # пока игровой цикл (как игры) работает
    if finish != True:
        # обновление координат
        player.update()  # передвижение игрока
        monster.update()  # передвижение противника

        window.blit(background, (0, 0))  # добавление фона на кадр
        player.reset()  # добавление игрока на кадр
        monster.reset()  # добавление врага на кадр
        final.reset()  # добавление сокровища на кадр

        # отрисовываем все сцены
        w1.draw_wall()
        w2.draw_wall()
        w3.draw_wall()
        w4.draw_wall()
        w5.draw_wall()
        w6.draw_wall()
        w7.draw_wall()

        # ситуация для проигрыша
        # проверка столкновения игрока со стенами или монстром
        if (
            sprite.collide_rect(player, w1)
            or sprite.collide_rect(player, w2)
            or sprite.collide_rect(player, w3)
            or sprite.collide_rect(player, w4)
            or sprite.collide_rect(player, w5)
            or sprite.collide_rect(player, w6)
            or sprite.collide_rect(player, w7)
            or sprite.collide_rect(player, monster)
        ):
            finish = True  # завершаем игру
            window.blit(lose, (200, 200))  # выводим надпись о проигрыше
            kick.play()  # проигрываем звук удара

        # ситуация для выигрыша
        if sprite.collide_rect(player, final):
            finish = True  # завершаем игру
            window.blit(win, (200, 200))  # выводим надпись о выигрыше
            money.play()  # проигрываем звук монет

    # обновляем все содержимое на экране
    display.update()
    clock.tick(FPS)
